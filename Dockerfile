#
# MAINTAINER        Nosix,nosix <six.heyaxiang@gmail.com>
# DOCKER-VERSION    1.6.2
#
# Dockerizing CentOS7: Dockerfile for building CentOS 1.7 images
#
FROM       centos:centos7.1.1503
MAINTAINER GuoPengfei <825338623@qq.com>

ENV TZ "Asia/Shanghai"
ENV TERM xterm

RUN yum install -y curl wget tar bzip2 unzip vim-enhanced passwd sudo yum-utils hostname net-tools rsync man && \
    yum clean all

#java配置
ADD jdk-7u80-linux-x64.tar.gz /usr/local/moji
ENV JAVA_HOME /usr/local/moji/jdk1.7.0_80
ENV CLASSPATH $JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $PATH:$JAVA_HOME/bin

#tomcat配置
ADD apache-tomcat-7.0.63.tar.gz /opt
RUN cd /opt/ && mv apache-tomcat-7.0.63 apache-tomcat
ENV CATALINA_HOME /opt/apache-tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
WORKDIR $CATALINA_HOME
  
EXPOSE 8080 22
 
CMD ["catalina.sh", "run"]