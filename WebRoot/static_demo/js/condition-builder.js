/**
 * 生成一个根目录
 * 
 * @param sel
 *            是一个DOM对象，类似于下面的格式 [ <div class="selected">
 *            <p>
 *            Hello Again
 *            </p>
 *            </div> ]
 * @param isroot
 *            是否是最初的根节点
 * 
 * @param json
 *            基础
 * 
 */
var addqueryroot = function(sel, isroot) {
	$(sel).append(rootVal);
	var q = $(sel).find('table'); // 返回标签为table的DOM对象(或由DOM对象组成的数组)

	var l = q.length;
	var elem = q;
	if (l > 1) {
		elem = $(q[l - 1]);
	}

	// 如果根元素则除去"*(红色的叉号)"的按钮
	if (isroot) {
		elem.find('td >#remove').detach();
	} else {
		elem.find('td >#remove').click(function() { // 绑定一个删除该组的事件
			// td>tr>tbody>table
			$(this).parent().parent().parent().parent().detach();
		});
	}

	// 在根条件添加默认的规则(每个div中的第一条规则)
	elem.find('td >#querystmts').append(firstCondition);

	// 在第一个规则中添加一个 “head” 属性
	elem.find('td >#querystmts div >#remove').addClass('head');

	// 点击"+(绿色的加号)"时添加新的规则，并且在这个新加的规则上添加一个当点击"*(红色的叉号)“时的事件
	elem.find('td div >#add')
			.click(
					function() {
						$(this).parent().siblings('#querystmts').append(
								firstCondition); // siblings():找到和此元素同辈的DOM对象，详见jquery.chm
						var stmts = $(this).parent().siblings('#querystmts')
								.find('div >#remove').filter(':not(.head)'); // 过滤，返回不包含"head"属性的DOM对象，详见jquery.chm
						stmts.unbind('click'); // 解除掉该DOM对象的click事件
						stmts.click(function() { // 点击时删除掉父类中所有的DOM对象(包含它自己)
							$(this).parent().detach();
						});
					});

	// 查找到”新组“这个按钮，并添加一个可以追加新的组的事件
	elem.find('td div > #addroot').click(function() {
		addqueryroot($(this).parent(), false);
	});
};

/**
 * 递归方法来解析和生成查询条件
 * 
 * @param rootsel
 *            一个未知的 table 节点(可能是一个嵌套的节点)
 * @returns {{}} 一个保存了当前选择的所有数据的json数据
 */
var getCondition = function(rootsel) {
	// 移除检查出错误的地方的事件监听
	if ($('#queryBuider >table').find(".error")) {
		var temp = $('#queryBuider >table').find(".error");
		if (temp.length > 0) {
			var errorNode = temp[0];
			errorNode.className = "right";
			removeEvent(errorNode);
		}
	}

	// 得到表的列(使用捷径来查找) tbody>tr>td
	var elem = $(rootsel).children().children().children(); // ele 是两个 td
	// ele[0] 是操作符(and 或则 or) DOM -- 连接符所在的td，ele[1] 是div中DOM --生成表达式 所在的td
	var q = {}; // q用来存储选定的数据和输入的数据有三个属性,operator ; expressions ;
	// nestedexpressions
	var expressions = []; // 表达式
	var nestedexpressions = []; // 嵌套的的表达式

	var operator = $(elem[0]).find(':selected').val(); // 选择的 and 或 or 值
	q.operator = operator;
	// 得到一个条件(and 或者 or)下的所有的表达式
	var expressionelem = $(elem[1]).find('> #querystmts div'); // 得到的是#querystmts下的所有的div(即所有的平行的查询条件，不包括嵌套的div)
	for (var i = 0; i < expressionelem.length; i++) {
		expressions[i] = {};

		var col = $(expressionelem[i]).find('#col :selected'); // 下拉DOM对象
		var op = $(expressionelem[i]).find('#op :selected'); // 下拉操作符DOM对象
		if (col.val() === '') {
			var errDom = $(col).parent().parent()[0];
			errDom.className = 'error';
			//			errorDom = errDom;
			currentMessage = errorMessage[0];
			bindEvent(errDom); // 显示浮动信息
			return false;
		}
		if (op.val() === '') {
			var errDom = $(op).parent().parent()[0];
			errDom.className = 'error';
			//			errorDom = errDom;
			currentMessage = errorMessage[1];
			bindEvent(errDom); // 显示浮动信息
			return false;
		}
		var editabel = $(expressionelem[i]).find(
				'input:checkbox[name="checkInput"]:checked').val();
		if (editabel == null) {
			expressions[i].editabel = '0';
		} else {
			expressions[i].editabel = '1';
		}
		expressions[i].colval = col.val(); // 条件下拉中选定的vale值

		expressions[i].coldisp = col.text(); // 条件下拉中选定的呈现给用户的值(label)
		expressions[i].opval = op.val(); // 操作符下拉中选定的value值
		expressions[i].opdisp = op.text(); // 操作符下拉中选定的呈现给用户的值

		// var a = [];
		if (op.val() === 'is null' || op.val() === 'is not null') {
			var tempVal = '{"valType":"is null"}';
			expressions[i].val = JSON.parse(tempVal); // 用户输入的值
			continue;
		}
		var tempVal = '{"values":[';
		var type = '';
		var thisNode = op.parent()[0].nextSibling; // 后面的所有的同辈的子节点
		while (thisNode) {
			// 级联下拉
			if (thisNode.name === allFrameName[0]) {
				if (thisNode.value === '') {
					var errDom = $(op).parent().parent()[0];
					errDom.className = 'error';
					//					errorDom = errDom;
					currentMessage = errorMessage[2];
					bindEvent(errDom); // 显示浮动信息
					return false;
				}
				for (var j = 0; j < thisNode.length; j++) {
					if (thisNode.options[j].selected) {
						tempVal += '"' + thisNode.options[j].value + '",';
					}
				}
				type = thisNode.name;
				break;

			}

			// radio输入
			if (thisNode.name === allFrameName[6]) {
				var val1 = $(thisNode).prev().nextAll(
						"input[name^='radioInupt']:checked").val();
				if (val1 == null) {
					var errDom = $(op).parent().parent()[0];
					errDom.className = 'error';
					//					errorDom = errDom;
					currentMessage = errorMessage[5];
					bindEvent(errDom); // 显示浮动信息
					return false;
				}
				tempVal += '"' + val1 + '",';
				type = thisNode.name;
				break;
			}
			// checkBox输入
			if (thisNode.name === allFrameName[7]) {
				var a = $(thisNode).prev();
				if (a.nextAll("input[name='checkInupt']:checkbox:checked").length > 0) {
					a.nextAll("input[name='checkInupt']:checkbox:checked")
							.each(function() {
								tempVal += $(this).val() + ',';
							});
					type = thisNode.name;
				} else {
					var errDom = $(op).parent().parent()[0];
					errDom.className = 'error';
					//						errorDom = errDom;
					currentMessage = errorMessage[6];
					bindEvent(errDom); // 显示浮动信息
					return false;
				}
				break;
			}

			// 判断处理输入框是否输入值
			if (thisNode.value.replace(/(^\s*)|(\s*$)/g, '') === '') {
				var errDom = $(op).parent().parent()[0];
				errDom.className = 'error';
				//				errorDom = errDom;
				currentMessage = errorMessage[3];
				bindEvent(errDom); // 显示浮动信息
				return false;
			}

			// 允许输入一个值的手动输入框
			if (thisNode.name === allFrameName[4]) {

				tempVal += '"' + thisNode.value + '",';
				type = thisNode.name;
				break;
			}
			// 允许输入多个值的手动输入框
			if (thisNode.name === allFrameName[5]) {
				var val = thisNode.value.replace(/(^\s*)|(\s*$)/g, '');

				var temp1 = val.split(";");

				for (m in temp1) {
					if (temp1[m].replace(/(^\s*)|(\s*$)/g, '') != '') {
						tempVal += '"' + temp1[m].replace(/(^\s*)|(\s*$)/g, '')
								+ '",';
					}
				}
				type = thisNode.name;
				break;
			}

			// 有两个操作框的情况，即如果操作符为between 或者 not between的
			if (op.val() === 'between' || op.val() === 'not between') {
				var las = $(thisNode).parent().find("input").last()[0];
				if (las.value.replace(/(^\s*)|(\s*$)/g, '') === '') {
					var errDom = $(op).parent().parent()[0];
					errDom.className = 'error';
					//					errorDom = errDom;
					currentMessage = errorMessage[4];
					bindEvent(errDom); // 显示浮动信息
					return false;
				}
				tempVal += '"' + thisNode.value + '",';
				tempVal += '"--","' + las.value + '",';
				type = thisNode.name;
				break;
			}
			// 只有一个输入框的情况
			tempVal += '"' + thisNode.value + '",';
			type = thisNode.name;
			break;

		}
		tempVal = tempVal.substring(0, tempVal.length - 1);
		tempVal += '],"valType":"' + type + '"}';
		var a = JSON.parse(tempVal);
		expressions[i].val = a; // 用户输入的值
		var allValueto = $(thisNode).parent().children("#hidden").val();
		var test_list = eval("(" + allValueto + ")");
		expressions[i].allValue = test_list;
	}
	q.expressions = expressions; // expressions包含五个属性 : colval; coldisp ;
	// opval ; opdisp ; val
	// 获取所有的嵌套的表达式
	if ($(elem[1]).find('> div > table').length != 0) {
		var len = $(elem[1]).find('> div > table').length;

		for (var k = 0; k < len; k++) {
			nestedexpressions[k] = getCondition($(elem[1])
					.find('> div > table')[k]);
		}
	}
	q.nestedexpressions = nestedexpressions; // 所嵌套的table中的数据
	return q;
};

/**
 * 递归的方法来遍历获取的查询条件（json），生成sql语句
 * 
 * @param condition
 *            上面的函数中解析出来的json对象 ，有三个参数: operator ; expressions ;
 *            nestedexpressions
 * @returns {string}
 */
var getQuery = function(condition) {
	var op = [ ' ', condition.operator, ' ' ].join(''); // join()
	// 方法用于把数组中的所有元素放入一个字符串。元素是通过指定的分隔符进行分隔的。operator
	// 为 and 或者 or
	var e = [];
	var elen = condition.expressions.length;
	for (var i = 0; i < elen; i++) {
		var expr = condition.expressions[i];
		var val = expr.val;
		var valType = val.valType;
		switch (valType) {
		case allFrameName[6]: {
			switch (expr.opval) {
			case '=':
			case '!=':
				e.push(expr.colval + " " + expr.opval + " '" + val.values[0]
						+ "'");
				break;
			default:
				e.push(expr.colval + " " + expr.opval); // 约束值为null 或者 is not null
			}
		}
			break;
		//var allFrameName = [ 'cascadeSele', 'int', 'date', 'decimal',
		//'singleStrselectFrame', 'multiStrselectFrame', 'radioInupt' ,'checkInupt'];
		case allFrameName[7]: {
			switch (expr.opval) {
			case 'in':
			case 'not in':
				var count = val.values.length;
				var temp = '(';
				for (var j = 0; j < count; j++) {
					temp += "'" + val.values[j] + "',";
				}
				temp = (temp.substring(0, temp.length - 1)) + ")";
				e.push(expr.colval + " " + expr.opval + " " + temp);
				break;
			default:
				e.push(expr.colval + " " + expr.opval); // 约束值为null 或者 is not null
			}
		}
			break;
		case 'int':
		case 'decimal': {
			switch (expr.opval) {
			case '=':
			case '!=':
			case '<=':
			case '>':
			case '>=':
				e.push(expr.colval + " " + expr.opval + " " + val.values[0]
						+ "");
				break;
			case 'between':
			case 'not between':

				e.push(expr.colval + " " + expr.opval + " '" + val.values[0]
						+ "' and '" + val.values[2] + "'");
				break;
			default:
				alert('在获取sql时，针对：' + type + '类型的值,针对' + expr.opval
						+ '类型的获取方法没有收录，');
			}
		}
			break;
		case 'date': {
			switch (expr.opval) {
			case '=':
			case '!=':
			case '<=':
			case '>':
			case '>=':
				e.push(expr.colval + " " + expr.opval + " to_date('"
						+ val.values[0] + "','yyyy-MM-dd') ");
				break;
			case 'between':
			case 'not between':

				e.push(expr.colval + " " + expr.opval + " to_date('"
						+ val.values[0] + "','yyyy-MM-dd') " + " and "
						+ " to_date('" + val.values[2] + "','yyyy-MM-dd') ");
				break;
			default:
				alert('在获取sql时，针对：' + type + '类型的值,针对' + expr.opval
						+ '类型的获取方法没有收录，');
			}
		}
			break;
		case 'multiStrselectFrame':
			var count = val.values.length;
			var temp = '(';
			for (var j = 0; j < count; j++) {
				temp += "'" + val.values[j] + "',";
			}
			temp = (temp.substring(0, temp.length - 1)) + ")";
			e.push(expr.colval + " " + expr.opval + " " + temp);
			break;
		case 'singleStrselectFrame': {
			switch (expr.opval) {
			case '=':
			case '!=':
				e.push(expr.colval + " " + expr.opval + " '" + val.values[0]
						+ "'");
				break;
			case 'like':
			case 'not like':
				e.push(expr.colval + " " + expr.opval + " '%" + val.values[0]
						+ "%' "); // push() 方法 可向数组的末尾添加一个或多个元素，并返回新的长度
				break;
			default:
				alert('在获取sql时，针对：' + type + '类型的值,针对' + expr.opval
						+ '类型的获取方法没有收录，');
			}
		}
			break;
		case 'cascadeSele':
			e.push(expr.colval + " " + expr.opval + " " + val.values[0] + "");
			break;

		case 'is null':
			e.push(expr.colval + " " + expr.opval); // 约束值为null 或者 is not null
			break;
		default:
			alert("此" + valType + "类型的json处理暂未收录");
		}
	}
	var n = [];
	var nlen = condition.nestedexpressions.length;
	for (var k = 0; k < nlen; k++) {
		var nestexpr = condition.nestedexpressions[k];
		var result = getQuery(nestexpr);
		n.push(result);
	}

	var q = [];
	if (e.length > 0)
		q.push(e.join(op));

	if (n.length > 0)
		q.push(n.join(op));

	return [ '(', q.join(op), ')' ].join(' ');
};

/**
 * 第一个下拉发生变化时触发
 * 
 * @param data
 *            当前触发的下拉
 */
function changeSelect(data) {
	// 首先移除此下拉后面的同辈的所有的节点

	var paratenNode = data.parentNode;
	while (data.nextSibling) {
		paratenNode.removeChild(data.nextSibling);
	}
	// 决定右侧显示的对象
	var value = data.value; // 包含别名和列
	var type = getColumType(value);
	switch (type)
	// ["StringType","TimestampType","DateType","CharacterType","ManyToOneType","IntegerType","decimal","OneToOneType"];
	{
	case allType[0]:
		$(data).parent().append(appendStringConstraint());
		break;
	case allType[1]:
	case allType[2]:
		$(data).parent().append(appendDateConstraint());
		break;
	case allType[3]:
		$(data).parent().append(appendRadioConstraint());
		break;
	case allType[4]:
		$(data).parent().append(appendFkonstraint());
		break;
	case allType[5]:
		$(data).parent().append(appendIntConstraint());
		break;
	case allType[6]:
		$(data).parent().append(appendIntConstraint());
		break;
	case allType[7]:
		alert("此" + type
				+ "类型未记录,在”condition_builder.js“文件中的第319行处添加关于此类型的处理方法！");
		break;
	default:
		alert("此" + type + "类型未记录");
	}
};

/**
 * 第二个下拉发生变化时决定后面追加的对象
 * 
 * @param data
 *            当前触发的对象
 * @param selectedData
 *            回显时记忆的查询值 为json 格式字符串内容
 */
function contraintSelect(data, selectedData) {
	//	alert("选中的链接条件为："+selectedData);
	// 首先移除此下拉后面的同辈的所有的节点
	var paratenNode = data.parentNode;
	while (data.nextSibling) {
		paratenNode.removeChild(data.nextSibling);
	}
	var preVal = data.previousSibling.value;
	var value = data.value;
	type = getColumType(preVal);
	// var allType =
	// ["StringType","TimestampType","DateType","CharacterType","ManyToOneType","int","decimal"];
	if (type === allType[1] || type === allType[2]) {
		// 日期类型的选项
		switch (value) {
		case "=":
		case ">=":
		case "<=":
		case "!=":
			$(data).parent().append(dateInputFrame());
			break;
		case "between":
		case 'not between':

			$(data).parent().append(dateInputFrame());
			$(data).parent().append("--");
			$(data).parent().append(dateInputFrame());
			break;
		case "is null":
			break;
		case "is not null":
			break;
		default:
			alert("没有添加此" + type + "类型关于" + value + "的值的输入方式");
		}
	} else if (type === allType[3]) {
		switch (value) {
		case "=":
		case "!=":
			//			currentType = type;
			//			currentOpr = allFrameName[6];
			//			currentData.push(data);
			makeSelection(preVal, selectedData, type, allFrameName[6]);
			break;

		case "in":
		case "not in":
			//			currentType = type;
			//			currentOpr = allFrameName[7];
			currentData.push(data);
			makeSelection(preVal, selectedData, type, allFrameName[7]);
			break;
		case "is null":
			break;
		case "is not null":
			break;
		default:
			alert("没有添加此" + type + "类型关于" + value + "的值的输入方式");
		}
	} else if (type === allType[5]) {
		switch (value) {
		case "=":
		case ">=":
		case "<=":
		case "!=":
			$(data).parent().append(intInputFrame());
			break;
		case "between":
		case "not between":

			$(data).parent().append(intInputFrame());
			$(data).parent().append("--");
			$(data).parent().append(intInputFrame());
			break;
		case "is null":
			break;
		case "is not null":
			break;
		default:
			alert("没有添加此" + type + "类型关于" + value + "的值的输入方式");
		}
	} else if (type === allType[6]) {
		switch (value) {
		case "=":
		case ">=":
		case "<=":
		case "!=":
			$(data).parent().append(decimalInputFrame());
			break;
		case "between":
		case "not between":
			$(data).parent().append(decimalInputFrame());
			$(data).parent().append("--");
			$(data).parent().append(decimalInputFrame());
			break;
		case "is null":
			break;
		case "is not null":
			break;
		default:
			alert("没有添加此" + type + "类型关于" + value + "的值的输入方式");
		}
	} else if (type === allType[0]) {
		// string 类型的有两种输入的形式，可多个输入，单个输入
		switch (value) {
		case "=":
		case "like":
		case "not like":
		case "!=":
			//			currentType = type;
			//			currentOpr = allFrameName[4];
			currentData.push(data);
			// 类和列的值：preVal ,约束值：value传递到后台处理，返回该类下的该列的所有的值构成的数组
			// 动态的从后台获取数据来作为数据填充到下面追加的下拉中
			makeSelection(preVal, selectedData, type, allFrameName[4]);

			break;
		case "in":
		case "not in":
			//			currentType = type;
			//			currentOpr = allFrameName[5];
			currentData.push(data);
			makeSelection(preVal, selectedData, type, allFrameName[5]);
			break;
		case "is null":
			break;
		case "is not null":
			break;
		default:
			alert("没有添加此" + type + "类型关于" + value + "的值的输入方式");
		}
	}
};
/**
 * 将约束条件和列传给后台bean，使其进行处理
 */
var makeSelection = function(val2, selectedData, dataType, currentOpr) {
	$.ajax({
		type : "POST",
		dataType : "json",
		url : "builder/queryBuilder.do",
		data : "selectedData=" + selectedData + "&val2=" + val2 + "&dataType="
				+ dataType + "&currentOpr=" + currentOpr,
		error : function(data) {
			alert(data);
		},
		success : function(data) {
			//        	alert("返回来的数据为：\n"+JSON.stringify(data));
			appentFrame(data.dataType, data.values, data.currentOpr,
					data.selecVal);
		}
	});
};

var appentFrame = function(datatype, values, currentOpr, lasSelect) {
	// 追加一个String类型的下拉框
	if (datatype === allType[0]) {
		if (currentOpr === allFrameName[4]) {
			var data = currentData.shift();
			$(data).parent().append(stringFrame(values, lasSelect));
		}
		if (currentOpr === allFrameName[5]) {
			var data = currentData.shift();
			$(data).parent().append(mutilStringFrame(values, lasSelect));
		}
	}
	//追加radio或checkbox块
	if (datatype === allType[3]) {
		if (currentOpr === allFrameName[6]) {
			var data = currentData.shift();
			$(data).parent().append(radioiInputFrame(values, lasSelect));
		}
		if (currentOpr === allFrameName[7]) {
			var data = currentData.shift();
			$(data).parent().append(multiCheckBoxFrame(values, lasSelect));
		}
	}
};
/**
 * 数字输入框只能输入数字
 */
function keyPress() {
	var keyCode = event.keyCode;
	if ((keyCode >= 48 && keyCode <= 57)) {
		event.returnValue = true;
	} else {
		event.returnValue = false;
	}
}

/**
 * 只允许输入数字和小数的判断方法
 * 
 * @param event
 * @param obj
 */
function clearNoNum(event, obj) {
	// 响应鼠标事件，允许左右方向键移动
	event = window.event || event;
	if (event.keyCode == 37 | event.keyCode == 39) {
		return;
	}
	// 先把非数字的都替换掉，除了数字和.
	obj.value = obj.value.replace(/[^\d.]/g, "");
	// 必须保证第一个为数字而不是.
	obj.value = obj.value.replace(/^\./g, "");
	// 保证只有出现一个.而没有多个.
	obj.value = obj.value.replace(/\.{2,}/g, ".");
	// 保证.只出现一次，而不能出现两次以上
	obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$",
			".");
};
function checkNum(obj) {
	// 为了去除最后一个.
	obj.value = obj.value.replace(/\.$/g, "");
};

/**
 * 将前端页面中生成的sql传给后台bean
 */
var setSql = function(val2, condition) {
	$.ajax({
		data : "sqlWhere=" + val2 + "&condition=" + condition,
		url : "builder/loadSQL.do",
		type : "POST",
		dataType : "json",
		success : function(data) {
			builderResult(data);
		},
		error : function(data) {
			alert("出错！");
		}
	});
};
/**
 * 
 * @param sel
 *            当前的节点
 * @param lastQueryJson
 *            存储的结构
 * @param isroot
 *            是不是根目录
 */
var reset = function(sel, lastQueryJson, isroot) {

	$(sel).append(rootVal);
	var q = $(sel).find('table'); // 返回标签为table的DOM对象(或由DOM对象组成的数组)
	var l = q.length;
	var elem = q;
	if (l > 1) {
		elem = $(q[l - 1]);
	}
	var opra = $(sel).find('#left select')[0];
	setSelectOption(opra, lastQueryJson.operator);

	// 如果根元素则除去"*(红色的叉号)"的按钮
	if (isroot) {
		elem.find('td >#remove').detach();
	} else {
		elem.find('td >#remove').click(function() { // 绑定一个删除该组的事件
			// td>tr>tbody>table
			$(this).parent().parent().parent().parent().detach();
		});
	}
	var expressions = lastQueryJson.expressions;
	for (var i = 0; i < expressions.length; i++) {
		// 在根条件添加默认的规则(每个div中的第一条规则)
		elem.find('td >#querystmts').append(firstCondition);
		// 如果是第一个规则，就把红色叉号的事件去掉，并加上head属性
		// 否则则添加上删除本行规则的监听事件
		if (i == 0) {
			elem.find('td >.querystmts div >.remove').addClass('head');
		} else {
			var stmts = elem.find('td >#querystmts div >#remove').filter(
					':not(.head)').last();
			stmts.unbind('click'); // 解除掉该DOM对象的click事件
			stmts.click(function() { // 点击时删除掉父类中所有的DOM对象(包含它自己)
				$(this).parent().detach();
			});
		}
		var curDiv = elem.find('td >#querystmts').children('#everyDiv').last();
		var firstSele = curDiv.find('select')[0];
		var editabel = expressions[i].editabel;
		if (editabel == 1) {
			var a1 = curDiv.find('input:checkbox[name="checkInput"]');
			a1.attr("checked", true);
		}
		setSelectOption(firstSele, expressions[i].colval);
		changeSelect(firstSele);
		// 不是级联下拉的情况
		if (getColumType((firstSele.value).substring(0, (firstSele.value)
				.lastIndexOf('.'))) != allType[4]) {
			firstSele = elem.find('td >#querystmts').children('#everyDiv')
					.last().find('select')[1];
			setSelectOption(firstSele, expressions[i].opval);
			// 在这里如果是后面要填充值是要从数据库中查询所得，则需要将上次选定的值传递后台，然后根据后台的处理来让后台返回的函数处理自动选定
			// 这是由于js函数和调用后台方法之间具有一个先后顺序，即js如果调用了后台程序，则它并不会等后台程序处理完再接着往下走，而是
			// 自己直接往下走，这就会造成上次选定的数据不能回显到下拉中
			if (expressions[i].opval != 'is null'
					&& expressions[i].opval != 'is not null') {
				var a = expressions[i].val.values;
				contraintSelect(firstSele, JSON.stringify(a));
			}
		}
		// 值要填充的话需要在此处写代码 var allFrameName =
		// ['cascadeSele','int','date','decimal','multipleSelect','singSele'];
		// 判断第三个组件的类型，根据类型填充选定的值
		firstSele = elem.find('td >#querystmts').children('#everyDiv').last()
				.find('select')[1].nextSibling;
		while (firstSele) {
			// 级联下拉
			if (firstSele.name === allFrameName[0]) {
				var val = expressions[i].val.values[0];
				setSelectOption(firstSele, val);
				break;
			}
			// 为date int decimal 输入形式的 ,这里面的值是在生成组件的时候固化进去的
			if (firstSele.name === allFrameName[1]
					|| firstSele.name === allFrameName[2]
					|| firstSele.name === allFrameName[3]) {
				var pre = firstSele.previousSibling;
				// 只有一个输入框的情况
				if (pre.value === '=' || pre.value === '<'
						|| pre.value === '<=' || pre.value === '>'
						|| pre.value === '>=' || pre.value === '!=') {
					var val = expressions[i].val.values[0];
					firstSele.value = val;
					break;
				}
				firstSele.value = expressions[i].val.values[0];
				firstSele.nextSibling.nextSibling.value = expressions[i].val.values[2];
				break;
			}
			alert("在回显查询数据时:" + firstSele.nodeName + "没有记录！");
			break;
		}

	}

	// 点击"+(绿色的加号)"时添加新的规则，并且在这个新加的规则上添加一个当点击"*(红色的叉号)“时的事件
	elem.find('td div >#add')
			.click(
					function() {
						$(this).parent().siblings('#querystmts').append(
								firstCondition); // siblings():找到和此元素同辈的DOM对象，详见jquery.chm
						var stmts = $(this).parent().siblings('#querystmts')
								.find('div >#remove').filter(':not(.head)'); // 过滤，返回不包含"head"属性的DOM对象，详见jquery.chm
						stmts.unbind('click'); // 解除掉该DOM对象的click事件
						stmts.click(function() { // 点击时删除掉父类中所有的DOM对象(包含它自己)
							$(this).parent().detach();
						});
					});

	// 查找到”新组“这个按钮，并添加一个可以追加新的组的事件
	elem.find('td div > #addroot').click(function() {
		addqueryroot($(this).parent(), false);
	});
	var nestedexpressions = lastQueryJson.nestedexpressions;
	if (nestedexpressions.length > 0) {
		var next = $(sel).find('td').find('div').find('> #addroot').parent();
		for (var a = 0; a < nestedexpressions.length; a++) {
			reset(next, lastQueryJson.nestedexpressions[a], false);
		}
	}

};

// 根据下拉对象默认选中后台对应的记录
/**
 * 将每条规则中的第一个下拉回显上次的选定的值
 * @param objSelect
 * @param targetValue
 * @returns {boolean}
 */
var setSelectOption = function(objSelect, targetValue) {
	if (objSelect) {
		var options = objSelect.options;
		if (options) {
			var len = options.length;
			for (var i = 0; i < len; i++) {
				if (options[i].value == targetValue) {
					options[i].defaultSelected = true;
					options[i].selected = true;
					return true;
				}
			}
		} else {
			alert('missing element(s)!');
		}
	} else {
		alert('missing element(s)!');
	}
};
/**
 * 将上次选定的值默认选定到下拉中
 * 
 * @param data
 *            当前下拉
 * @param selecVal
 *            当前下拉中选定的值
 */
var selecTraverse = function(data, selecVal) {
	for (var i = 0; i < selecVal.length; i++) {
		setSelectOption(data, selecVal[i]);
	}
};
/**
 * 重新生成查询条件
 */
var getQueryRoot = function(lastQuery) {

	if (lastQuery != null && lastQuery != '') {
		lastJson = eval("(" + lastQuery + ")");
		reset('#queryBuider', lastQuery, true);
	} else {
		addqueryroot('#queryBuider', true);
	}
};
/**
 * 获取光标在input中的位置
 * 
 * @param auto
 *            提示信息显示的地方
 * @param data
 *            DOM对象
 * @param mylist
 *            json对象，要提示的信息
 */
function AutoComplete(auto, data, mylist) {

	auto.empty(); // 清空上次的记录
	var oldValue = '';
	var count = '';
	if (data.id === allFrameName[5]) {
		// 多选的情况
		if (data.value.lastIndexOf("; ") > 0) {
			count = data.value.lastIndexOf("; ");
			oldValue = data.value.substring(count + 2, data.value.length)
					.replace(/(^\s*)|(\s*$)/g, "");
		} else {
			oldValue = data.value.replace(/(^\s*)|(\s*$)/g, "");
		}

	}
	if (data.id === allFrameName[4]) {
		oldValue = data.value.replace(/(^\s*)|(\s*$)/g, "");
	}
	function getTop(e) {
		var offset = e.offsetTop;
		if (e.offsetParent != null)
			offset += getTop(e.offsetParent);
		return offset;
	}
	;
	// 获取元素的横坐标
	function getLeft(e) {
		var offset = e.offsetLeft;
		if (e.offsetParent != null)
			offset += getLeft(e.offsetParent);
		return offset;
	}
	;
	function getWidth(e) {
		var w = $(e).width();
		return w - 4;
	}
	// var autoNode = auto; //缓存对象（弹出框）
	var t = getTop(data) + data.clientHeight;
	var l = getLeft(data);
	var w = getWidth(data);
	//	var framePos = getLablePosition(data);
	//	var top1 = t+"px";
	//	var left1 = l+ "px";
	auto.css({
		"top" : (t + 2) + "px",
		"left" : (l - 8) + "px"
	});

	var carlist = new Array();
	var n = 0;
	for (i in mylist) {
		if (mylist[i].indexOf(oldValue) >= 0) {
			carlist[n++] = mylist[i];
		}
	}
	if (carlist.length == 0) {
		auto.hide();
		return;
	}
	for (i in carlist) {
		var wordNode = carlist[i]; // 弹出框里的每一条内容
		var newDivNode = $("<div>").attr("id", i); // 设置每个节点的id值
		newDivNode
				.attr(
						"style",
						"font:14px/25px arial;height:25px;width:"
								+ w
								+ "px;padding:0 8px;cursor: pointer;white-space:nowrap;text-overflow:ellipsis;overflow: hidden;");
		newDivNode.html(wordNode).appendTo(auto); // 追加到弹出框
		// 鼠标移入高亮，移开不高亮
		newDivNode.mouseover(function() {
			if (highlightindex != -1) { // 原来高亮的节点要取消高亮（是-1就不需要了）
				auto.children("div").eq(highlightindex).css("background-color",
						"white");
			}
			// 记录新的高亮节点索引
			highlightindex = $(this).attr("id");
			$(this).css("background-color", "#ebebeb");
		});
		newDivNode.mouseout(function() {
			$(this).css("background-color", "white");
		});
		// 鼠标点击文字上屏
		newDivNode
				.click(function() {
					// 取出高亮节点的文本内容
					var comText = auto.hide().children("div")
							.eq(highlightindex).text();
					highlightindex = -1;
					if (data.id == allFrameName[5]) {
						var temp = data.value.substring(0, count + 2);
						data.value = temp + comText + "; ";
					}
					if (data.id == allFrameName[4]) {
						data.value = comText;
					}
					data.focus();
				});
		if (carlist.length > 0) { // 如果返回值有内容就显示出来
			auto.show();
		} else { // 服务器端无内容返回 那么隐藏弹出框
			auto.hide();
			// 弹出框隐藏的同时，高亮节点索引值也变成-1
			highlightindex = -1;
		}
	}
	// 点击页面隐藏自动补全提示框
	document.onclick = function(event) {
		var e = event ? event : window.event;
		var tar = e.srcElement || e.target;
		if (tar.id != data.id) {
			if (auto.is(":visible")) {
				auto.css("display", "none");
			}
		}
	};

};
/**
 * 当光标聚焦此处时
 * 
 * @param data
 *            当前DOM对象
 */
function onFocus(data) {
	if ($(data).val() == "") {
		var value = $(data).siblings("input").val();
		var test_list = eval("(" + value + ")");

		AutoComplete($("#auto_div"), data, test_list);
	}
};
/**
 * 监听键盘松开
 * 
 * @param input
 *            DOM对象
 */
function onKeyUp(data) {
	var value = $(data).siblings("input").val();
	if (value != 'null') {
		var test_list = eval("(" + value + ")");
		AutoComplete($("#auto_div"), data, test_list);
	}
};

/**
 * 根据光标所在位置选中部分文字
 * 
 * @param input
 *            DOM对象
 */
function getComponentValue(data) {
	tx = data;
	te = new TextareaEditor(tx);
	var pos = te.getSelection();
	var temp = pos.selectionStart;
	var valuetemp = data.value;
	if (temp === 0 || temp === valuetemp.length) {
		var value = $(data).nextAll("input").val();
		var test_list = eval("(" + value + ")");

		AutoComplete($("#auto_div"), data, test_list);
		return;
	}
	var begin = '';
	var end = '';

	if (temp != 0) {
		if (temp == valuetemp.length) {
			return;
		}
	}
	for (var i = temp; i >= 0; i--) {
		if (i == 0 || valuetemp.charAt(i - 1) === ' ') {
			begin = i;
			break;
		}
	}
	for (var i = temp; i <= valuetemp.length; i++) {
		if (i + 1 == valuetemp.length || valuetemp.charAt(i) === ' ') {
			end = i + 1;
			break;
		}
	}
	te.setSelectionRange(begin, end);
};
/**
 * 监听键盘按下的哪个键
 * 
 * @param data
 *            DOM对象
 * @param 事件
 */
function getKey(data, event) {
	tx = data;
	te = new TextareaEditor(tx);
	var evt = window.event || event;
	if (evt.keyCode == 13 || evt.keyCode == 108 || evt.keyCode == 32
			|| evt.keyCode == 186) {
		te.insertData("; ");
		evt.keyCode = 0;
		evt.returnValue = false;
	}
};
/**
 * 获取光标在input中的位置
 * 
 * @param input
 *            DOM对象
 * @returns {Object} 位置坐标
 */
function getPosition(input) {
	var pos = 0;
	if (document.selection) {
		input.focus();
		var selection = document.selection.createRange();
		selection.moveStart('character', -input.value.length);
		pos = selection.text.length;
	} else if (input.selectionStart || input.selectionStart == '0')
		pos = input.selectionStart;
	return pos;
};
/**
 * 获取dom对象在窗口的位置
 * 
 * @param data
 *            DOM对象
 * @returns {Object} json对象，包含DOM的坐标信息
 */
function getLablePosition(data) {
	var t = data.offsetTop;
	var l = data.offsetLeft;
	var h = data.offsetHeight;
	var w = data.offsetWidth;
	while (data = data.offsetParent) {
		t += data.offsetTop;
		l += data.offsetLeft;
	}
	var point = eval("({x:" + l + ",y:" + t + ",h:" + h + ",w:" + w + "})");
	return point;
};

var builderResult = function(data) {
	var title = data.colums;

	var table = $(".hovertable");
	table.empty();
	var tr = $("<tr>");
	for (var i = 0; i < title.length; i++) {
		var th = $("<th>");
		th.html(title[i]);
		th.appendTo(tr);
	}
	tr.appendTo(table);
	var values = data.values;
	for (var m = 0; m < values.length; m++) {
		var tr1 = $("<tr>");
		tr1.addClass("trTable");
		var data = values[m];
		var value = data.values;
		for (var n = 0; n < value.length; n++) {
			var td = $("<td>");
			td.html(value[n]);
			td.appendTo(tr1);
		}
		tr1.appendTo(table);
	}
	$(".trTable").mouseover(function() {
		$(this).css("background-color", "#ffff66");
	});
	$(".trTable").mouseout(function() {
		$(this).css("background-color", "#d4e3e5");
	});
};