<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
    String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<%=path %>/css/images/ico.ico" type="image/x-icon" /> 
<title>QueryBuilder-查询模板</title>
<link href="<%=path%>/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="<%=path%>/css/base.css" rel="stylesheet" type="text/css" />
<link href="<%=path%>/css/queryTemplate.css" rel="stylesheet"
	type="text/css" />
<!-- 公共的css样式 -->
<link href="<%=path%>/css/base.css" rel="stylesheet" type="text/css" />
<!-- 警告框样式 -->
<link href="<%=path%>/css/sweetalert.css" rel="stylesheet"
	type="text/css" />
<script type="text/javascript" src="<%=path%>/js/path.js"></script>
<!-- 警告框js -->
<script src="<%=path%>/js/sweetalert.min.js"></script>

<script type="text/javascript" src="<%=path%>/js/jquery.min.js"></script>
<script type="text/javascript" src="<%=path%>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.validate.min.js"></script>
</head>

<body>
	<ul class="nav nav-tabs" id="query_ul">
		<li role="presentation" id="query_template"  class="active"><a
			href="<%=path %>/jumpDemo">模板管理</a></li>
		<li role="presentation" id="builder_query"><a
			href="<%=path %>/builderQuery">查询结构管理</a></li>
		<li role="presentation" id="query_result"><a
			href="<%=path %>/queryResult">查询结果管理</a></li>
	</ul>
	<div class="dis_pro">
		<input type="hidden" id="path" value="<%=path%>" /> 

			<input
			type="hidden" id="option" /> <input type="hidden" id="opRow" />
	</div>
	<div class="tbtree">
		<div class="tbopera">
			<div class="tbbtns">
				<a href="#">刷新</a><a href="#" data-toggle="modal"
					onclick="add_or_update_template('')">添加新模板</a>
			</div>
			<script>
				function test() {
					var tmpName = "";
					// 新增查询模板
					$("#option").val("add");
					var initData = getClassFromServer(tmpName);

					var pick = $("#pickList").pickList(initData);
				}
			</script>

			<div class="tbsearch">
				<input class="inptext" type="text" name="" id="" value="" /><a
					href="#">搜索</a>
			</div>
		</div>
		<div class="tbtable">
			<table id="templateData" width="100%" border="0" cellspacing="0"
				cellpadding="0">

			</table>
		</div>
	</div>

	<div id="os_copy" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2>
					编辑查询模板 <a class="ico_close" href="javascript:void(0)"
						onclick="closePop('os_copy','cp_os_ul')"></a>
				</h2>
				<div class="popdiv_cnt">
					<ul class="ipfp" id="cp_os_ul">
						<li><b>模板名称</b>
							<div class="ipfpcn">
								<input type="text" id="tmp_in" class="inptext"
									placeholder="请填写模板名称" aria-describedby="basic-addon1">
							</div></li>
						<li>
							<div id="pickList"></div>
						</li>
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)"
						id="getSelected">确定</a>
				</div>
			</div>
		</div>

	</div>

	<div id="alia_add" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2>
					新增别名 <a class="ico_close" href="javascript:void(0)"
						onclick="closePop('alia_add','alia_ul')"></a>
				</h2>
				<div class="popdiv_cnt">
					<ul class="ipfp" id="alia_ul">
						<li><b>别名</b>
							<div class="ipfpcn">
								<input type="text" id="alia_in" class="inptext"
									placeholder="请填写别名" aria-describedby="basic-addon1">
							</div></li>
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)" id="addAlia">确定</a>
				</div>
			</div>
		</div>

	</div>

	<div id="alia_edit" style="display:none">
		<div class="popdiv-mask">
			<div class="popdiv">
				<h2>
					编辑查询结果显示列 <a class="ico_close" id="close_edit_alia"
						href="javascript:void(0)"></a>
				</h2>
				<div class="popdiv_cnt">
					<ul class="ipfp" id="ed_al_ul">
						<li><b>别名</b>
							<div class="ipfpcn">
								<input type="text" id="alia_edit_in" class="inptext"
									placeholder="请填写查询类别名" aria-describedby="basic-addon1">
							</div></li>
						<li>
							<div class="performinfo clearfix" id="tab4">
								<div id="pro_source" class="perinfo_l fl">
									<ul>
										<li><a href="#">shell1.sh</a></li>
										<li class="current"><a href="#">shell2.sh</a></li>
										<li><a href="#">shell3.sh</a></li>
										<li><a href="#">shell4.sh</a></li>
									</ul>
								</div>
								<div id="pro_target" class="perinfo_l fr">
									<ul>
										<li><a href="#">shell1.sh</a></li>
										<li><a href="#">shell2.sh</a></li>
										<li><a href="#">shell3.sh</a></li>
										<li><a href="#">shell4.sh</a></li>
									</ul>
								</div>
							</div>
						</li>
						<!-- <li class="pop_error">错误提示</li> -->
					</ul>
				</div>
				<div class="popdiv_btn">
					<a class="btnblue bodrad5" href="javascript:void(0)"
						id="alia_submit">确定</a>
				</div>
			</div>
		</div>

	</div>


	<script type="text/javascript" src="<%=path%>/js/pickList.js"></script>
	<script type="text/javascript" src="<%=path%>/js/template.js?ver=1.1"></script>
	<!--footer-->
	<div class="jq_foot">
		<span class="mr30">版权所有 © 2016</span>QueryBuilder
	</div>
</body>
</html>
